package DBConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBHandler {
	
	Connection dbconnection;
	Configs obj = new Configs();
	
	public Connection getConnection() {
		
		String connectionString = "jdbc:mysql://" + obj.dbhost + ":" + obj.dbport + "/" +obj.dbname;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
		
		try {
			dbconnection = DriverManager.getConnection(connectionString, obj.dbuser,obj.dbpassword );
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return this.dbconnection;
	}

	
}
