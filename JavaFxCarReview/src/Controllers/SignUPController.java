package Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javax.net.ssl.HandshakeCompletedListener;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;

import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;
import DBConnection.DBHandler;

public class SignUPController implements Initializable{
	
	@FXML
    private JFXTextField txtUsername;
	
	@FXML
	private ToggleGroup genders;
    
	@FXML
    private JFXTextField passPassword;

    @FXML
    private JFXRadioButton rBtnMale;

    @FXML
    private JFXRadioButton rBtnFemale;

    @FXML
    private JFXTextField txtLocation;

    @FXML
    private JFXButton btnSignUP;

    @FXML
    private ImageView iViewUsername;

    @FXML
    private ImageView iViewPassword;

    @FXML
    private ImageView iViewGender;

    @FXML
    private JFXButton btnLogin;

    @FXML
    private ImageView iViewCar;

    @FXML
    private Label lbl1;

    @FXML
    private Label lbl2;

    @FXML
    private ImageView iViewLoadingGIF;
    
    private Connection connection;
    private DBHandler handler;
    private PreparedStatement pst;

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.iViewLoadingGIF.setVisible(false);
		this.txtUsername.setStyle("-fx-text-inner-color: #D0D2DA");
		this.passPassword.setStyle("-fx-text-inner-color: #D0D2DA");
		this.txtLocation.setStyle("-fx-text-inner-color: #D0D2DA");
		
		this.handler = new DBHandler();	
	}
	
	@FXML
	public void loginAction (ActionEvent event) throws IOException {
		
		this.btnLogin.getScene().getWindow().hide();
		Stage login = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/FXML/LoginMain.fxml"));
		Scene scene = new Scene(root,686,469);
		login.setScene(scene);
		login.show();
		login.setResizable(false);
	}
	
	public void signUPAction(ActionEvent event) {
		
		iViewLoadingGIF.setVisible(true);
		PauseTransition pt = new PauseTransition();
		pt.setDuration(Duration.seconds(3));
		pt.setOnFinished(e -> {
			System.out.println("Sign Up succesfull");
		});
		pt.play();
		
		// Saving Data
		
		String insert = "INSERT INTO Users(names,password,gender,location)"
				+ "VALUES (?,?,?,?)";
		
		this.connection = this.handler.getConnection();
		try {
			pst = connection.prepareStatement(insert);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		try {
			pst.setString(1, this.txtUsername.getText());
			pst.setString(2, this.passPassword.getText());
			pst.setString(3, getGender());
			pst.setString(4, this.txtLocation.getText());
			
			pst.executeUpdate();
			
			
			
		} catch (SQLException e1) {
		
			e1.printStackTrace();
		}
		
		
	}
	
	public String getGender() {
		String gen = "";
		
		if(this.rBtnMale.isSelected()) {
			gen = "Male";
		}
		else if (this.rBtnFemale.isSelected()){
			gen ="Female";
		}
		return gen;
	}
	
	
	
}
