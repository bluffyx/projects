package Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.mysql.jdbc.PreparedStatement;

import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;
import DBConnection.DBHandler;

public class LoginController implements Initializable {
	
		private DBHandler handler;
		private Connection connection;
		private java.sql.PreparedStatement pst;
	
	 	@FXML
	    private JFXTextField txtUsername;

	    @FXML
	    private JFXPasswordField passPassword;

	    @FXML
	    private JFXButton btnSignUP;

	    @FXML
	    private JFXButton btnLogin;

	    @FXML
	    private JFXCheckBox cboxRemeberMe;

	    @FXML
	    private JFXButton btnForgotYourPassword;

	    @FXML
	    private ImageView iViewCar;

	    @FXML
	    private ImageView iViewForgotYPass;

	    @FXML
	    private ImageView iViewUsername;

	    @FXML
	    private ImageView iViewPassword;

	    @FXML
	    private Label lbl1;

	    @FXML
	    private Label lbl2;

	    @FXML
	    private ImageView iViewLoadingGIF;
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		this.iViewLoadingGIF.setVisible(false);
		this.txtUsername.setStyle("-fx-text-inner-color: #D0D2DA");
		this.passPassword.setStyle("-fx-text-inner-color: #D0D2DA");
		
		this.handler = new DBHandler();
		
	}
	
	@FXML
	public void loginAction(ActionEvent event) {
		
		iViewLoadingGIF.setVisible(true);
		PauseTransition pt = new PauseTransition();
		pt.setDuration(Duration.seconds(3));
		pt.setOnFinished(ev -> {
			
			System.out.println("Login Succesfull");
		});
		pt.play();
		
		//Retrieve data from databse
		
		this.connection = handler.getConnection();
		String q1 = "SELECT * from Users WHERE names=? and password=?";
		
		try {
			pst = connection.prepareStatement(q1);
			pst.setString(1, this.txtUsername.getText());
			pst.setString(2, this.passPassword.getText());
			ResultSet rs = pst.executeQuery();
			
			int count=0;
			while (rs.next()) { //true if the new current row is valid; false if there are no more rows 
				count = count+1;
			}
			if (count==1) {
				System.out.println("Login successfull !");
			} else {
				System.out.println("Username and Password is not Correct");
			}
			
			
		} catch (SQLException e) {
			
			e.printStackTrace(); 
		}
		finally {
			try {
				connection.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		}
	}
	
	@FXML
	public void signUp(ActionEvent event) throws IOException {
		
		this.btnLogin.getScene().getWindow().hide();
		Stage signUP = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/FXML/SignUP.fxml"));
		Scene scene = new Scene(root,686,572);
		signUP.setScene(scene);
		signUP.show();
		signUP.setResizable(false);
			
	}
	
	
	
}
